package cr.ac.ucr.ecci.cql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;
    // Lenguaje
    private String mLenguaje = "en";
    private Locale mLocale = Locale.US;
    // Texto y botones iniciales de la actividad principal
    TextView mTexto;
    // Opcion de Voz a texto
    Button mSpeechText;
    // Lista de textos encontrados
    ArrayList<String> mMatchesText;
    // Lista para mostrar los textos
    ListView mTextListView ;
    // Dialogo para mostrar la lista
    Dialog mMatchTextDialog;
    // Instancia de Texto a voz
    TextToSpeech mTextToSpeech;
    // Texto de entrada
    EditText mEditText;
    // Opcion de Texto a voz
    Button mTextSpeech;

    Translate translate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getTranslateService();

        // componentes de la aplicacion
        mSpeechText = (Button) findViewById(R.id.buttonSpeechText);
        mTextSpeech = (Button) findViewById(R.id.buttonTextSpeech);
        mTexto = (TextView) findViewById(R.id.texto);
        mEditText = (EditText) findViewById(R.id.editText);
        // intent para el reconocimiento de voz
        mSpeechText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isConnected ()){
                    // intent al API de reconocimiento de voz
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

                    //Para que se detecte el lenguaje contrario al que se desea traducir, esto para el método de voz.
                    String languageHelper = "en";
                    if(mLenguaje == "en"){
                        languageHelper = "es";
                    }

                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE , languageHelper);
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "No hay conexión a Internet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Set el lenguaje de texto a voz
        mTextToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit (int status) {
                mTextToSpeech.setLanguage(mLocale);
            }
        });

        // Intent para texto a voz
        mTextSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // lenguaje
                mTextToSpeech.setLanguage(mLocale);
                // Texto el edit
                String toSpeak = mEditText.getText().toString();
                String translatedInput = translate(toSpeak);
                Toast.makeText(getApplicationContext(), translatedInput, Toast.LENGTH_SHORT).show();

                mTextToSpeech.speak(translatedInput, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });
    }

    // evento se seleccion de idioma
    public void onRadioButtonClicked(View view) {
        // Verificar el RadioButton seleccionado
        boolean checked = ((RadioButton) view).isChecked();
        // Determinar cual RadioButton esta seleccionado
        switch(view.getId()) {
            case R.id.radioButtonIngles:
                if (checked)
                    mLenguaje = "en";
                mLocale = Locale.US;
                break;
            case R.id.radioButtonEspannol:
                if (checked)
                    mLenguaje = "es";
                mLocale = new Locale("spa", "ESP");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Valores de retorn del intent
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK ) {
            // Si retorna resultados el servicion de reconocimiento de voz creamos el dialogo y asignamos la lista
            mMatchTextDialog = new Dialog(MainActivity.this);
            mMatchTextDialog.setContentView(R.layout.dialog_matches_frag);
            // título del dialogo
            mMatchTextDialog.setTitle("Seleccione el texto");
            // Lista de elementos
            mTextListView = (ListView) mMatchTextDialog.findViewById(R.id.listView1);
            mMatchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            // Mostramos los datos en la lista
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mMatchesText);
            mTextListView.setAdapter(adapter);
            // Asignamos el evento del clic en la lista
            mTextListView.setOnItemClickListener(new AdapterView.OnItemClickListener () {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String translatedInput = translate(mMatchesText.get(position));
                    mTexto.setText("You have said: " + translatedInput);
                    mEditText.setText(translatedInput);
                    mMatchTextDialog.hide();
                    mMatchTextDialog.dismiss();
                }});
            mMatchTextDialog.show();
        }
    }

    // verificar conexion a internet
    public boolean isConnected (){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if(net != null && net.isAvailable() && net.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public void getTranslateService() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try (InputStream is = getResources().openRawResource(R.raw.credentials)) {
            //Get credentials:
            final GoogleCredentials myCredentials = GoogleCredentials.fromStream(is);

            //Set credentials and get translate service:
            TranslateOptions translateOptions = TranslateOptions.newBuilder().setCredentials(myCredentials).build();
            translate = translateOptions.getService();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public String translate(String originalInput) {
        Translation translation = translate.translate(originalInput, Translate.TranslateOption.targetLanguage(mLenguaje), Translate.TranslateOption.model("base"));
        String translatedText = translation.getTranslatedText();
        return translatedText;
    }



}